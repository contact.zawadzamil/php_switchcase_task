<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Switch Case Example</title>
</head>

<?php
$input = 80;
$output = '';
switch($input){
    case ($input > 0 && $input < 33):
        $output = 'You Have Failed!';
        break;

    case ($input >= 33 && $input < 40):
        $output = 'You Got: D';
        break;

    case ($input >= 40 && $input < 50):
        $output = 'You Got: C';
        break;

    case ($input >= 50 && $input < 60):
        $output = 'You Got: B';
        break;

    case ($input >= 60 && $input < 70):
        $output = 'You Got: A-';
        break; 
    
    case ($input >= 70 && $input < 80):
        $output = 'You Got: A';
        break;
    
    case ($input >= 80 && $input < 100):
        $output = 'You Got: A+';
        break;
    default:
        $output = 'Invalid Input!';
}

?>
<body>
    <h1 >
        Switch Case Example
    </h1>

    <h3>You Entered: <?php echo $input ?> </h3>
    <h2>  <?php echo $output ?> </h2>


    
</body>
<style>
    h1,h3,h2{
        text-align: center;
       
    }
    h1{
        color:red;
    }
</style>
</html>   